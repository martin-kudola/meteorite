package com.nasa.meteorite;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

/**
 * Created by Jean Sarter on 28/04/2017.
 */
public interface Interface {

    //This method is used for "GET"
    @GET("/resource/y77d-th95.json?$$app_token=Xokpp8ww9PVSUxKOZQDtrnhNu")
    Call<List<ServerResponse>> getMeteoriteDetails();
}
