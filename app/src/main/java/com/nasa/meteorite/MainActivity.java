package com.nasa.meteorite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;
import okhttp3.OkHttpClient;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Intent _intent = null;
    private RecyclerView _recyclerView;
    private ProgressBar _mProgressBar;
    private boolean _isCheckedYear = false;
    private MenuItem sort_by_year;
    private List<MeteoriteModel> _Meteorites = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        _mProgressBar = (ProgressBar)findViewById(R.id.loading_spinner);
        _recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        _recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, _recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //TODO what ever you want: like starting an Activity
                if (!MeteoriteModel.isDataListEmpty()) {
                    try {
                        _intent = new Intent(Intent.ACTION_VIEW);
                        _intent.setData(Uri.parse("geo:" + _Meteorites.get(position).get_latitude() + "," + _Meteorites.get(position).get_longitude() + "?q=" + _Meteorites.get(position).get_latitude() +", "+ _Meteorites.get(position).get_longitude() +"("+ _Meteorites.get(position).get_title() +")&z=10"));
                        startActivity(_intent);

                    } catch(Exception ex) {
                        Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "No meteorite.", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onLongClick(View view, int position) {
                //TODO what ever you want: like starting an Activity
                Toast.makeText(MainActivity.this, "onLongClick at position: " + position, Toast.LENGTH_SHORT).show();
            }
        }));

        /*
         * Lolipop, Marshmallow, Nougat and higher version
         */
        getMeteorites();

        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        _recyclerView.setLayoutManager(layoutManager);
        _recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private static class ApiManager {
        private static final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://data.nasa.gov")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        private static Retrofit getRetrofit() {
            return retrofit;
        }
    }

    private void getMeteorites() {
        Retrofit retrofit = ApiManager.getRetrofit();

        Interface service = retrofit.create(Interface.class);
        retrofit2.Call<List<ServerResponse>> call = service.getMeteoriteDetails();
        call.enqueue(new Callback<List<ServerResponse>>() {
            @Override
            public void onResponse(retrofit2.Call<List<ServerResponse>> call, Response<List<ServerResponse>> response) {
                if (response.body() != null) {
                    List<ServerResponse> meteorites = response.body();
                    MeteoriteAdapter adapter = new MeteoriteAdapter(MainActivity.this,MeteoriteModel.getObjectList(meteorites));
                    _recyclerView.setAdapter(adapter);

                    _Meteorites = MeteoriteModel.getObjectList(meteorites);
                    _mProgressBar.setVisibility(View.GONE);

                  /*
                   * Store Meteorites name,latitude,longitude and year of landings into Shared Preferences.
                   */
                    MeteoriteModel.storeMeteorites(getApplicationContext());
                }
            }

            @Override
            public void onFailure(retrofit2.Call<List<ServerResponse>> call, Throwable t) {

                /*
                 * Load Meteorites data from Shared Preferences.
                 */
                MeteoriteAdapter adapter = new MeteoriteAdapter(MainActivity.this,MeteoriteModel.loadMeteorites(getApplicationContext()));
                _recyclerView.setAdapter(adapter);

                _Meteorites = MeteoriteModel.loadMeteorites(getApplicationContext());
                _mProgressBar.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {

            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onClick(child, recyclerView.getChildAdapterPosition(child));
                    }

                    return super.onSingleTapUp(e);
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    //super.onLongPress(e);
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                // Click recyclerview at given position
                clickListener.onClick(child, rv.getChildAdapterPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            //TODO use interface ClickListener in some way, to trigger methods onClick() and onLongClick() at right moment based on what hapend.
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            //TODO use interface ClickListener in some way, to trigger methods onClick() and onLongClick() at right moment based on what hapend.
        }
    }

    public static interface ClickListener {
        public void onClick(View view, int position);
        public void onLongClick(View view, int position);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem action_search = menu.findItem(R.id.action_search);
        sort_by_year = menu.findItem(R.id.sort_by_year);
        SearchView searchView = (SearchView) action_search.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                _isCheckedYear = sort_by_year.isChecked();
                if (_isCheckedYear) {
                    sortQueryTextYear(s,1);
                } else {
                    sortQueryTextYear(s,2);
                }
                sort_by_year.setChecked(_isCheckedYear);

                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                _isCheckedYear = sort_by_year.isChecked();
                if (_isCheckedYear) {
                    sortQueryTextYear(s,1);
                } else {
                    sortQueryTextYear(s,2);
                }
                sort_by_year.setChecked(_isCheckedYear);
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id) {
            case R.id.sort_by_year:
                _isCheckedYear = !item.isChecked();
                if (_isCheckedYear) {
                    sortYear(1);
                } else {
                    sortYear(2);
                }
                item.setChecked(_isCheckedYear);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void sortQueryTextYear(String s,int sortOrder) {
        MeteoriteAdapter adapter = new MeteoriteAdapter(MainActivity.this,MeteoriteModel.sortQueryTextYear(s,sortOrder));
        _recyclerView.setAdapter(adapter);
        adapter.updateFilterChanges();
        _Meteorites = MeteoriteModel.sortQueryTextYear(s,sortOrder);
    }

    public void sortYear(int sortOrder) {
        MeteoriteAdapter adapter = new MeteoriteAdapter(MainActivity.this,MeteoriteModel.sortYear(sortOrder));
        _recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        _Meteorites = MeteoriteModel.sortYear(sortOrder);
    }

}
