package com.nasa.meteorite;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Jean Sarter on 23/04/2017.
 */
public class MeteoriteAdapter extends RecyclerView.Adapter<MeteoriteAdapter.MyViewHolder> {

    private List<MeteoriteModel> _objectList;
    private LayoutInflater _inflater;
    private Context _context;

    public MeteoriteAdapter(Context context, List<MeteoriteModel> objectList) {
        this._inflater = LayoutInflater.from(context);
        this._objectList = objectList;
        this._context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = _inflater.inflate(R.layout.list_tem, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MeteoriteModel current = _objectList.get(position);
        holder.setData(current, position);
    }

    @Override
    public int getItemCount() {
        return _objectList.size();
    }

    public void updateFilterChanges() {
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView _title;
        private TextView _titleGeoposition;
        private TextView _mass;
        private TextView _yearLanding;
        private int _position;
        private MeteoriteModel _currentObject;

        public MyViewHolder(View itemView) {
            super(itemView);
            _title = (TextView)itemView.findViewById(R.id.title_meteorite);
            _titleGeoposition = (TextView)itemView.findViewById(R.id.geoposition_meteorite);
            _mass = (TextView)itemView.findViewById(R.id.mass);
            _yearLanding = (TextView)itemView.findViewById(R.id.year_landing);
        }

        public void setData(MeteoriteModel current, int position) {
            // TODO setting data for list Item
            _title.setText(current.get_title());
            _titleGeoposition.setText("Latitude: " + current.get_latitude() + "\nLongitude: " + current.get_longitude());

            if (current.get_mass() != null) {
                _mass.setText("( weight " + Double.valueOf(current.get_mass())/1000 + " [kg] )");
            } else {
                _mass.setText("( weight " + current.get_mass() + " [?] )");
            }

            if (current.get_yearLanding() != null) {
                _yearLanding.setText("( Year " + current.get_yearLanding().substring(0,4) + " )");
            } else {
                _yearLanding.setText("( Year " + current.get_yearLanding() + " )");
            }

            _position = position;
            _currentObject = current;
        }
    }
}
