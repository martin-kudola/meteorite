package com.nasa.meteorite;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;
import com.google.gson.Gson;

import java.util.*;

/**
 * Created by Jean Sarter on 23/04/2017.
 */
public class MeteoriteModel {

    private String _title;
    private String _latitude;
    private String _longitude;
    private String _yearLanding;
    private String _mass;

    private static List<MeteoriteModel> dataList = new ArrayList<>();
    private static List<MeteoriteModel> dataListSearchFilter = new ArrayList<>();

    public String get_mass() {
        return _mass;
    }

    public void set_mass(String mass) {
        this._mass = mass;
    }

    public String get_yearLanding() {
        return _yearLanding;
    }

    public void set_yearLanding(String yearLanding) {
        this._yearLanding = yearLanding;
    }

    public String get_title() {
        return _title;
    }

    public void set_title(String title) {
        this._title = title;
    }

    public String get_longitude() {
        return _longitude;
    }

    public void set_longitude(String longitude) {
        this._longitude = longitude;
    }

    public String get_latitude() {
        return _latitude;
    }

    public void set_latitude(String latitude) {
        this._latitude = latitude;
    }

    public static List<MeteoriteModel> getObjectList(List<ServerResponse> meteorites) {
        dataList.clear();

        for (int i = 0; i < meteorites.size(); i++) {
            MeteoriteModel meteor = new MeteoriteModel();

            meteor.set_title(meteorites.get(i).getName());
            meteor.set_latitude(meteorites.get(i).getReclat());
            meteor.set_longitude(meteorites.get(i).getReclong());
            meteor.set_yearLanding(meteorites.get(i).getYear());
            meteor.set_mass(meteorites.get(i).getMass());

            dataList.add(meteor);
        }
        return dataList;
    }

    public static String getLatitude(int position) {
        return dataList.get(position).get_latitude();
    }

    public static String getLongitude(int position) {
        return dataList.get(position).get_longitude();
    }

    public static String getMeteoriteName(int position) {
        return dataList.get(position).get_title();
    }

    public static void storeMeteorites(Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences("Meteorite_apk",Context.MODE_PRIVATE);
        editor = settings.edit();
        Gson gson = new Gson();
        String jsonMeteoriteName = gson.toJson(dataList);
        editor.putString("Meteorites_dataList", jsonMeteoriteName);
        editor.apply();
    }

    public static List<MeteoriteModel> loadMeteorites(Context context) {
        MeteoriteModel meteor = new MeteoriteModel();
        SharedPreferences settings;
        settings = context.getSharedPreferences("Meteorite_apk",Context.MODE_PRIVATE);
        if (settings.contains("Meteorites_dataList")) {
            String jsonFavorites = settings.getString("Meteorites_dataList", null);
            Gson gson = new Gson();
            MeteoriteModel[] dataListItems = gson.fromJson(jsonFavorites,MeteoriteModel[].class);
            dataList = Arrays.asList(dataListItems);

        } else {
            dataList.clear();
            meteor.set_title("Empty list of meteorites.\nThere is no Shared\nPreferences");
            meteor.set_latitude("0");
            meteor.set_longitude("0");
            meteor.set_yearLanding("0000");
            dataList.add(meteor);
        }

        return  dataList;
    }

    public static boolean isDataListEmpty() {
        return dataList.get(0).get_title().equals("Empty list of meteorites.\nThere is no Shared\nPreferences");
    }

    public static List<MeteoriteModel> sortQueryTextYear(String s, int sortOrder) {
        dataListSearchFilter.clear();
        String meteoriteSearch = s.toLowerCase();
        for (MeteoriteModel aDataList : dataList) {
            if (aDataList.get_title().toLowerCase().contains(meteoriteSearch) || (aDataList.get_yearLanding() != null && aDataList.get_yearLanding().contains(meteoriteSearch)) ) {
                dataListSearchFilter.add(aDataList);
            }
        }
        Collections.sort(dataListSearchFilter, new sortYear(sortOrder));
        return dataListSearchFilter;
    }

    private static class sortYear implements java.util.Comparator<MeteoriteModel> {

        private int _sortOrder;

        public sortYear(int sortOrder) {
            this._sortOrder = sortOrder;
        }

        @Override
        public int compare(MeteoriteModel o, MeteoriteModel t1) {
            if (_sortOrder == 1) {

                /*
                * Ascending
                */
                if (o.get_yearLanding() == null) {
                    return (t1.get_yearLanding() == null) ? 0 : -1;
                }
                if (t1.get_yearLanding() == null) {
                    return 1;
                }
                return (o.get_yearLanding()).compareTo( t1.get_yearLanding() );

            } else {

                /*
                 * Descending
                 */
                if (o.get_yearLanding() == null) {
                    return (t1.get_yearLanding() == null) ? 0 : -1;
                }
                if (t1.get_yearLanding() == null) {
                    return 1;
                }
                return (t1.get_yearLanding()).compareTo( o.get_yearLanding() );
            }
        }
    }

    public static List<MeteoriteModel> sortYear(int sortOrder) {
        if (dataListSearchFilter.size() > 0 ) {
            Collections.sort(dataListSearchFilter, new sortYear(sortOrder));
            return dataListSearchFilter;
        } else {
            Collections.sort(dataList, new sortYear(sortOrder));
            return dataList;
        }
    }
}
