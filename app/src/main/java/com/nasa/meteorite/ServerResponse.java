package com.nasa.meteorite;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Jean Sarter on 07/04/2017.
 */
public class ServerResponse implements Serializable {

    public String fall;
    public Geolocation geolocation;
    public String id;
    public String mass;
    public String name;
    public String nametype;
    public String recclass;
    public String reclat;
    public String reclong;
    public String year;

    public ServerResponse(String fall, Geolocation geolocation, String id, String mass, String name, String nametype, String recclass, String reclat, String reclong, String year){
        this.fall = fall;
        this.geolocation = geolocation;
        this.id = id;
        this.mass = mass;
        this.name = name;
        this.nametype = nametype;
        this.recclass = recclass;
        this.reclat = reclat;
        this.reclong = reclong;
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public void setYear() {
        this.year = year;
    }

    public String getReclong() {
        return reclong;
    }

    public void setReclong() {
        this.reclong = reclong;
    }

    public String getReclat() {
        return reclat;
    }

    public void setReclat(String reclat) {
        this.reclat = reclat;
    }

    public String getRecclass() {
        return recclass;
    }

    public void setRecclass(String recclass) {
        this.recclass = recclass;
    }

    public String getNametype() {
        return nametype;
    }

    public void setNametype(String nametype) {
        this.nametype = nametype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMass() {
        return mass;
    }

    public void setMass(String mass) {
        this.mass = mass;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Geolocation getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(Geolocation geolocation) {
        this.geolocation = geolocation;
    }

    public String getFall() {
        return fall;
    }

    public void setFall(String fall) {
        this.fall = fall;
    }


    public static class Geolocation {
        public String type;
        public double[] coordinates;

        public Geolocation(String type, double[] coordinates){
            this.type = type;
            this.coordinates = coordinates;
        }

        public String getType() {
            return  type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public double[] getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(double[] coordinates) {
            this.coordinates = coordinates;
        }
    }
}

